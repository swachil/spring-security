# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
Create a configuration class where Spring Security configurations are created. In this project configuration class is SecurityConfiguration.java
* Dependencies
In this project we have used Spring security, Thymeleaf, Spring Data JPA, Spring Dev Tools and Spring Web dependencies.
* Database configuration
We have used MySQL DB having schema named "springsecurity".
* How to run tests
* Deployment instructions
You just need to run application and hit localhost:18091/ api.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact