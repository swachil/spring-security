package com.springsecurity.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.springsecurity.demo.model.CustomSecurityUser;
import com.springsecurity.demo.model.User;
import com.springsecurity.demo.repository.UserRepository;

@Service
public class UserServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		User user = userRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("Username/Password not available in system.");
		}
		return new CustomSecurityUser(user);
	}

	@Secured({ "ROLE_USER" })
	public List<User> getAllUserDetails() {
		return userRepository.findAll();
	}

}
