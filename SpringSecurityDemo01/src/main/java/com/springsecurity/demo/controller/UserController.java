package com.springsecurity.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.springsecurity.demo.model.User;
import com.springsecurity.demo.service.UserServiceImpl;

@Controller
public class UserController {

	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserServiceImpl userService;

	@GetMapping("/dashboard")
	public String getDashboard(@AuthenticationPrincipal User user, ModelMap model) {
		model.put("user", user);
		logger.info("User Name: {}", user.getUsername());
		List<User> allUserDetails = userService.getAllUserDetails();
		logger.info("User Details: {}", allUserDetails);
		return "dashboard";
	}

	@GetMapping("/error")
	public String getError(@AuthenticationPrincipal User user) {
		logger.error("Error occured while login with username: {}", user.getUsername());
		return "error";
	}
}
